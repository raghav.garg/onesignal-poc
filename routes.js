const express = require('express');

module.exports = function() {

  // initiaizing express router
  const router = express.Router();

  router.use( function(req, res, next) {
    console.log('middleware');
    next()
  });

  router.route('/').get(function(req, res) {
    console.log('hello world');
    return res.status(200).send('hello world')
  });

  // return instance of router so that it can be added to the express instance
  return router;

}
