const app = require('express')();

const server = require('./server');
const routes = require('./routes');

app.use('/', routes());

const settings = {
  ssl: {
    active: true,
    key: 'server.key',
    certificate: 'server.crt'
  },
  port: 3000
};

server.create(settings, app, function () {
  console.log('Started!');
});
